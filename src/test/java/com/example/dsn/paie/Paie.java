package com.example.dsn.paie;

import lombok.Builder;
import lombok.Data;

//
//import lombok.Builder;
//import lombok.Data;
//
//@Builder
//@Data
//public class Paie {
//    String CodeRubrique ;
//    String DescriptionAssiette;
//    float assiette;
//    String formuleAssiete;
//    String formuleMontantSalarie;
//    String formuleMontantPatronale;
//    String formuletauxSalarial;
//    String type;
//    float tauxSalarial;
//    float montantSalarial;
//    float tauxPatronal;
//    float montantPatronal;
//}
@Builder
@Data
public class Paie {
    String CodeRubrique ;
    String type;
    String LibelleRubrique;
    String formuleAssiete;
    String CodificationRubrique;
    String Retenu_Gain;

    String formuletauxSalarial;
    String formuletauxPatronal;
    String formuleMontantSalarial;
    String formuleMontantPatronal;
    String DescriptionAssiette;

    double assiette;

    public String getCodeRubrique() {
        return CodeRubrique;
    }

    public void setCodeRubrique(String codeRubrique) {
        CodeRubrique = codeRubrique;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLibelleRubrique() {
        return LibelleRubrique;
    }

    public void setLibelleRubrique(String libelleRubrique) {
        LibelleRubrique = libelleRubrique;
    }

    public String getFormuleAssiete() {
        return formuleAssiete;
    }

    public void setFormuleAssiete(String formuleAssiete) {
        this.formuleAssiete = formuleAssiete;
    }

    public String getCodificationRubrique() {
        return CodificationRubrique;
    }

    public void setCodificationRubrique(String codificationRubrique) {
        CodificationRubrique = codificationRubrique;
    }

    public String getRetenu_Gain() {
        return Retenu_Gain;
    }

    public void setRetenu_Gain(String retenu_Gain) {
        Retenu_Gain = retenu_Gain;
    }

    public String getFormuletauxSalarial() {
        return formuletauxSalarial;
    }

    public void setFormuletauxSalarial(String formuletauxSalarial) {
        this.formuletauxSalarial = formuletauxSalarial;
    }

    public String getFormuletauxPatronal() {
        return formuletauxPatronal;
    }

    public void setFormuletauxPatronal(String formuletauxPatronal) {
        this.formuletauxPatronal = formuletauxPatronal;
    }

    public String getFormuleMontantSalarial() {
        return formuleMontantSalarial;
    }

    public void setFormuleMontantSalarial(String formuleMontantSalarial) {
        this.formuleMontantSalarial = formuleMontantSalarial;
    }

    public String getFormuleMontantPatronal() {
        return formuleMontantPatronal;
    }

    public void setFormuleMontantPatronal(String formuleMontantPatronal) {
        this.formuleMontantPatronal = formuleMontantPatronal;
    }

    public String getDescriptionAssiette() {
        return DescriptionAssiette;
    }

    public void setDescriptionAssiette(String descriptionAssiette) {
        DescriptionAssiette = descriptionAssiette;
    }

    public double getAssiette() {
        return assiette;
    }

    public void setAssiette(double assiette) {
        this.assiette = assiette;
    }

    public double getTauxSalarial() {
        return tauxSalarial;
    }

    public void setTauxSalarial(double tauxSalarial) {
        this.tauxSalarial = tauxSalarial;
    }

    public double getMontantSalarial() {
        return montantSalarial;
    }

    public void setMontantSalarial(double montantSalarial) {
        this.montantSalarial = montantSalarial;
    }

    public double getTauxPatronal() {
        return tauxPatronal;
    }

    public void setTauxPatronal(double tauxPatronal) {
        this.tauxPatronal = tauxPatronal;
    }

    public double getMontantPatronal() {
        return montantPatronal;
    }

    public void setMontantPatronal(double montantPatronal) {
        this.montantPatronal = montantPatronal;
    }

    double tauxSalarial;
    double montantSalarial;
    double tauxPatronal;
    double montantPatronal;


    public String toStringCsv() {
        return "" +
                "" + this.CodeRubrique + '\'' +
                ", '" + this.type + '\'' +
                ", '" + this.LibelleRubrique + '\'' +
                ", '" + this.formuleAssiete + '\'' +
                ", '" + this.CodificationRubrique + '\'' +
                ", '" + this.Retenu_Gain + '\'' +
                ", '" + this.formuletauxSalarial + '\'' +
                ", '" + this.formuletauxPatronal + '\'' +
                ", '" + this.formuleMontantSalarial + '\'' +
                ", '" + this.formuleMontantPatronal + '\'' +
                ", '" + this.DescriptionAssiette + '\'' +
                "," + this.assiette +
                ", " + this.tauxSalarial +
                ", " + this.montantSalarial +
                ", " + this.tauxPatronal +
                "," + this.montantPatronal ;
    }
}