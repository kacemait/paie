package com.example.dsn.paie;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class test {
    @Test
    public  void calculBulltinPaie()
    {
        Param param = Param.builder().PMSS(3428)
                .SMIC(1603.12)
                .ETAB_NB_SAL(1)
                .NB_HR_ETAB(1)
                .NB_JR_OUVRE_MOY(21.67)
                .SAL_MENS(4500)
                .TAUX_PAS(12.8)
                .PRIME_VACANCES(100)
                .PRIME_BRUT_1(120)
                .PRIME_BRUT_2(150)
                .AVANTAGE_NATURE_1(200)
                .AVANTAGE_NATURE_2(250)
                .AVANTAGE_NATURE_3(300)
                .AVANTAGE_NATURE_4(350)
                .NB_HS_25_EMPLOYE(1.25)
                .NB_HS_50_EMPLOYE(1.5)
                .NB_PRESENCE_CAL(31)
                .NB_MOIS_CAL(31)
                .NB_CPN(2.08)
                .NB_CPN1(0)
                .NB_CPN2(0)
                .NB_ANCI_N(0)
                .NB_ANCI_N1(0)
                .NB_CP_PRIS(1)
                .NB_CP_INDEM(1.75)
                .NB_MAL_PRIS(3)
                .NB_MAL_INDEM(2)
                .NB_AT_PRIS(4)
                .NB_AT_INDEM(3)
                .NB_IJSS_TOT(2.5)
                .NB_IJSS_NET(2.5)
                .NB_JR_DEPLACEMENT_FRAIS_KM(21)
                .DISTANCE_DOMICILE_MISSION(10)
                .FRAIS_KM_CV(0.603)
                .DIV_NET(550).build();
        List<Paie> rubriquesPaie = new ArrayList();
        FileReader filereader = null;
        try {
            URL csv = getClass().getClassLoader().getResource("rubriques.csv");
            filereader = new FileReader(csv.getFile());
            //filereader = new FileReader("/Users/winu/Downloads/listes_rubriques_20220215.csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // create csvParser object with
        // custom separator semi-colon
        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

        // create csvReader object with parameter
        // filereader and parser
        CSVReader csvReader = new CSVReaderBuilder(filereader)
                .withCSVParser(parser)
                .build();


        try (CSVReader reader =  new CSVReaderBuilder(filereader)
                .withCSVParser(parser)
                .build();) {
            List<String[]> r = reader.readAll();
            //codeRubrique;type;libelleRubrique;assiete;codificationRubrique;retenu_Gain;formuletauxSalarial;taux_salarial;montantSalarial;formuletauxPatronal;taux_patronal;montantPatronal
            r.forEach(x -> {
                        System.out.println(
                                Arrays.asList(x)
                        );
                        Paie rubrique =null;
                        //TODO pour skip la premiere ligne
                        if(!x[0].contains("codeRubrique") && x[0]!=null && !x[0].equals("") )
                        {

                            rubrique = Paie.builder().CodeRubrique(x[4])
                                    //.DescriptionAssiette(x[2])
                                    .formuleAssiete(x[3])
                                    .formuleMontantPatronal(x[11])
                                    .type(x[1])
                                    .formuleMontantSalarial(x[8])
                                    .LibelleRubrique(x[2])
                                    .formuletauxSalarial(x[6])
                                    .tauxSalarial(Float.parseFloat(x[7]))
                                    .tauxPatronal(Float.parseFloat(x[10]))
                                    .formuletauxPatronal(x[9])
                                    .montantSalarial(-1)
                                    .montantPatronal(-1)
                                    .assiette(-1)
                                    .build();
                            rubriquesPaie.add(rubrique);
                        }

                    }
            );
			/*PMSS,SMIC,ETAB_NB_SAL,NB_HR_ETAB,NB_JR_OUVRE_MOY,SAL_MENS,TAUX_PAS,PRIME_VACANCES,PRIME_BRUT_1,PRIME_BRUT_2,AVANTAGE_NATURE_1,AVANTAGE_NATURE_2,AVANTAGE_NATURE_3,AVANTAGE_NATURE_4,NB_HS_25_EMPLOYE,NB_HS_50_EMPLOYE,NB_PRESENCE_CAL,NB_MOIS_CAL,NB_CPN,NB_CPN1,NB_CPN2,NB_ANCI_N,NB_ANCI_N1,NB_CP_PRIS,NB_CP_INDEM,NB_MAL_PRIS,NB_MAL_INDEM,NB_AT_PRIS,NB_AT_INDEM,NB_IJSS_TOT,NB_IJSS_NET,NB_JR_DEPLACEMENT_FRAIS_KM,DISTANCE_DOMICILE_MISSION,FRAIS_KM_CV,DIV_NET
			3428,1603.12,1,1,21.67,4500,12.8,100,120,150,200,250,300,350,1.25,1.5,31,31,2.08,0,0,0,0,1,1.75,3,2,4,3,2.5,2.5,21,10,0.603,550*/
            //System.out.println(rubriquesPaie.size());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvException e) {
            e.printStackTrace();
        }
        //TODO calcul Assiete
        rubriquesPaie.stream().forEach(p->p=this.calculAssiete(p,rubriquesPaie,param));
        //TODO calcul taux salarial
        rubriquesPaie.stream().forEach(p->p=this.calculTauxSalarial(p,rubriquesPaie,param));
        //TODO calcul taux patronal
        //TODO calcul montant salarial
        rubriquesPaie.stream().forEach(p->p=this.montantSalarial(p,rubriquesPaie,param));
        //TODO calcul montant patronal
        rubriquesPaie.stream().forEach(p->p=this.montantPatronal(p,rubriquesPaie,param));
       //AFFICHER tous les objects
        rubriquesPaie.stream().forEach(p-> System.out.println(p.toStringCsv()+"\\n"));


    }
    public Paie calculAssiete(Paie rubrique , List<Paie> rubriquesPaie, Param param)
    {
        double brute = calculBrute(rubriquesPaie,param);
        double BruteRetablieMois = calculBruteRetablieMois(rubriquesPaie,param);
        switch (rubrique.getFormuleAssiete()) {
            case "NB_HR_ETAB" -> {
                rubrique.setAssiette(param.getNB_HR_ETAB());
            }

            case "NB_HS_25_EMPLOYE" -> {
                rubrique.setAssiette(param.getNB_HS_25_EMPLOYE());
            }
            case "NB_HS_50_EMPLOYE" -> {
                rubrique.setAssiette(param.getNB_HS_50_EMPLOYE());
            }
            case "NB_CP_PRIS" -> {
                rubrique.setAssiette(param.getNB_CP_PRIS());
            }
            case "NB_CP_INDEM" -> {
                rubrique.setAssiette(param.getNB_CP_INDEM());
            }
            case "NB_MAL_PRIS" -> {
                rubrique.setAssiette(param.getNB_MAL_PRIS());
            }
            case "NB_MAL_INDEM" -> {
                rubrique.setAssiette(param.getNB_MAL_INDEM());
            }
            case "NB_AT_PRIS" -> {
                rubrique.setAssiette(param.getNB_AT_PRIS());
            }
            case "NB_AT_INDEM" -> {
                rubrique.setAssiette(param.getNB_AT_INDEM());
            }
            case "NB_IJSS_TOT" -> {
                rubrique.setAssiette(param.getNB_IJSS_TOT());
            }
            case "NB_IJSS_NET" -> {
                rubrique.setAssiette(param.getNB_IJSS_NET());
            }

            case "BRUT_RETAB_MOIS" -> {
                rubrique.setAssiette(BruteRetablieMois);
            }

            case "BRUT_TOTAL" -> {
                rubrique.setAssiette(brute);
            }



            case "IF : (BRUT_TOTAL <= 2.5*SMIC); Then BRUT_TOTAL"-> {
                if(brute<= (2.5 * param.getSMIC()))
                    rubrique.setAssiette(brute);
                else
                    rubrique.setAssiette(0);
            }
            case "Min (BRUT_TOTAL , PMSS)"-> {
                rubrique.setAssiette(Math.min(brute,param.getPMSS()));
            }
            case "BRUT_TOTAL - Min (BRUT_TOTAL , PMSS)"-> {
                rubrique.setAssiette(brute-Math.min(brute,param.getPMSS()));
            }
            case "IF : (BRUT_TOTAL <= 3.5*SMIC); Then BRUT_TOTAL"-> {
                if(brute<= (3.5 * param.getSMIC()))
                    rubrique.setAssiette(brute);
                else
                    rubrique.setAssiette(0);
            }
            case "Min (BRUT_TOTAL, 4*PMSS)"-> {
                rubrique.setAssiette(brute-Math.min(brute,4*param.getPMSS()));
            }
            case "IF : (ETAB_NB_SAL  > 11  ); Then BRUT_TOTAL"-> {
                if(param.getETAB_NB_SAL ()>11)
                    rubrique.setAssiette(brute);
                else
                    rubrique.setAssiette(0);
            }
            case "IF : (ETAB_NB_SAL  > 50 ); Then BRUT_TOTAL"-> {
                if(param.getETAB_NB_SAL ()>50)
                    rubrique.setAssiette(brute);
                else
                    rubrique.setAssiette(0);
            }
            case "98,28%*(BRUT_TOTAL) + SANTE_PREV_A:montant_patronal + SANTE_PREV_B:montant_patronal + SANTE_MUT_OBL:montant_patronal"-> {
                Optional<Paie> santePrevA = rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_PREV_A")).findFirst();
                Optional<Paie> santePrevB = rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_PREV_B")).findFirst();
                Optional<Paie> santeMutObl = rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_MUT_OBL")).findFirst();
                double santePrevAMontantSalarial = this.montantSalarial(santePrevA.get(), rubriquesPaie, param).getMontantSalarial();
                double santePrevBMontantSalarial = this.montantSalarial(santePrevB.get(), rubriquesPaie, param).getMontantSalarial();
                double santeMutOblMontantSalarial = this.montantSalarial(santeMutObl.get(), rubriquesPaie, param).getMontantSalarial();
                if(santePrevA.get().getMontantSalarial()!=-1 && santePrevB.get().getMontantSalarial()!=-1 && santeMutObl.get().getMontantSalarial()!=-1)
                    rubrique.setAssiette((double) ((brute*0.9828)+santePrevAMontantSalarial+santePrevBMontantSalarial+santeMutOblMontantSalarial));
            }

            case "NB_JR_DEPLACEMENT_FRAIS_KM" -> {
                rubrique.setAssiette(param.getNB_JR_DEPLACEMENT_FRAIS_KM());
            }

            case "NET_IMPOSABLE" -> {
                double NET_IMPOSABLE = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("NET_IMPOSABLE")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
            }

        };
        return rubrique;
    }

    private double calculBrute(List<Paie> rubriquesPaie , Param param) {
        Optional<Paie> paie = rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("BRUT_TOTAL")).findFirst();
        if(paie.get().getMontantSalarial()==-1)
        {
            return  this.montantSalarial(paie.get(), rubriquesPaie,param).getMontantSalarial();
        }else {
            return  paie.get().getMontantSalarial();
        }

    }

    private double calculBruteRetablieMois(List<Paie> rubriquesPaie , Param param) {
        Optional<Paie> paie = rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("BRUT_RETAB_MOIS")).findFirst();
        if(paie.get().getMontantSalarial()==-1)
        {
            return  this.montantSalarial(paie.get(), rubriquesPaie,param).getMontantSalarial();
        }else {
            return  paie.get().getMontantSalarial();
        }

    }

    public void calculTauxPatronal(Paie rubrique ,List<Paie> rubriquesPaie, Param param)
    {
        switch (rubrique.getFormuletauxPatronal()) {

            case ""-> {

            }
        };
    }

    public Paie calculTauxSalarial(Paie rubrique ,List<Paie> rubriquesPaie, Param param)
    {
        switch (rubrique.getFormuletauxSalarial()) {

            case "SAL_MENS/NB_HR_ETAB"-> {

                rubrique.setTauxSalarial(param.getSAL_MENS()/param.getNB_HR_ETAB());

            }

            case "1.25*SAL_MENS/NB_HR_ETAB"-> {

                rubrique.setTauxSalarial((double) (1.25 * (param.getSAL_MENS()/param.getNB_HR_ETAB())));

            }

            case "1.5*SAL_MENS/NB_HR_ETAB"-> {

                rubrique.setTauxSalarial((double) (1.5 * (param.getSAL_MENS()/param.getNB_HR_ETAB())));

            }

            case "SAL_MENS/NB_JR_OUVRE_MOY"-> {

                rubrique.setTauxSalarial(param.getSAL_MENS()/param.getNB_JR_OUVRE_MOY());

            }

            case "SAL_MENS/NB_MOIS_CAL"-> {

                rubrique.setTauxSalarial(param.getSAL_MENS()/param.getNB_MOIS_CAL());

            }

            case "(3*0.5*BRUT_RETAB_MOIS)/91.25"-> {

                rubrique.setTauxSalarial((double) ((3*0.5*this.calculBruteRetablieMois(rubriquesPaie,  param))/91.25));

            }

            case "DISTANCE_DOMICILE_MISSION*FRAIS_KM_CV"-> {

                rubrique.setTauxSalarial(param.getDISTANCE_DOMICILE_MISSION()*param.getFRAIS_KM_CV());

            }

            case "TAUX_PAS"-> {

                rubrique.setTauxSalarial(param.getTAUX_PAS());

            }




        };
        return rubrique;
    }

    public Paie montantPatronal(Paie rubrique ,List<Paie> rubriquesPaie, Param param)
    {
        switch (rubrique.getFormuleMontantPatronal()) {
            case "assiette* taux_patronal" -> {
                rubrique.setMontantPatronal(rubrique.getAssiette()*rubrique.getTauxPatronal());
            }
            case "URSSAF_ASS_MAL_MAT_INV_DEC:montant_patronal + URSSAF_ASS_MAL_MAT_INV_DEC_COMP:montant_patronal + SANTE_PREV_A:montant_patronal + SANTE_PREV_B:montant_patronal + SANTE_MUT_OBL:montant_patronal + URSSAF_ACC_TRAV:montant_patronal + URSSAF_ASS_VIE_PLA:montant_patronal + URSSAF_ASS_VIE_DEPLA:montant_patronal + RET_AGIRC_ARRCO_T1:montant_patronal + RET_AGIRC_ARRCO_T2:montant_patronal + RET_CEG_T1:montant_patronal + RET_CEG_T2:montant_patronal + RET_CET:montant_patronal + URSSAF_ALLOC_FAMI:montant_patronal + URSSAF_ALLOC_FAMI_COMP:montant_patronal + URSSAF_COTIS_CHOM:montant_patronal + URSSAF_COTIS_AGS:montant_patronal + RET_APEC:montant_patronal + URSSAF_FNAL:montant_patronal + URSSAF_CONTR_SOLIDARITE_AUTO:montant_patronal + DIV_VER_TRANS:montant_patronal + DIV_PART_EFFORT_CONSTR:montant_patronal + URSSAF_CONTR_DIA_SOCI:montant_patronal + URSSAF_FORFA_SOC:montant_patronal + FOR_CONTR_UNIQUE_FPRO_APP:montant_patronal + FOR_CONTR_SUP_APP:montant_patronal + CCN_CONTR_ADESATT:montant_patronal + URSSAF_CSG_NO_IMP:montant_patronal + URSSAF_CSG_IMP:montant_patronal + URSSAF_CRDS:montant_patronal + REINT_FISCAL:montant_patronal"-> {

                double URSSAF_ASS_MAL_MAT_INV_DEC = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_MAL_MAT_INV_DEC")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_ASS_MAL_MAT_INV_DEC_COMP = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_MAL_MAT_INV_DEC_COMP")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double SANTE_PREV_A = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_PREV_A")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double SANTE_PREV_B = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_PREV_B")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double SANTE_MUT_OBL = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_MUT_OBL")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_ACC_TRAV = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ACC_TRAV")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_ASS_VIE_PLA = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_VIE_PLA")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_ASS_VIE_DEPLA = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_VIE_DEPLA")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double RET_AGIRC_ARRCO_T1 = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_AGIRC_ARRCO_T1")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double RET_AGIRC_ARRCO_T2 = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_AGIRC_ARRCO_T2")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double RET_CEG_T1 = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_CEG_T1")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double RET_CEG_T2 = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_CEG_T2")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double RET_CET = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_CET")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_ALLOC_FAMI = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ALLOC_FAMI")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_ALLOC_FAMI_COMP = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ALLOC_FAMI_COMP")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_COTIS_CHOM = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_COTIS_CHOM")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_COTIS_AGS = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_COTIS_AGS")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double RET_APEC = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_APEC")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_FNAL = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_FNAL")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_CONTR_SOLIDARITE_AUTO = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CONTR_SOLIDARITE_AUTO")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double DIV_VER_TRANS = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("DIV_VER_TRANS")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double DIV_PART_EFFORT_CONSTR = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("DIV_PART_EFFORT_CONSTR")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_CONTR_DIA_SOCI = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CONTR_DIA_SOCI")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_FORFA_SOC = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_FORFA_SOC")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double FOR_CONTR_UNIQUE_FPRO_APP = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("FOR_CONTR_UNIQUE_FPRO_APP")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double FOR_CONTR_SUP_APP = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("FOR_CONTR_SUP_APP")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double CCN_CONTR_ADESATT = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("CCN_CONTR_ADESATT")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_CSG_NO_IMP = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CSG_NO_IMP")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_CSG_IMP = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CSG_IMP")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double URSSAF_CRDS = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CRDS")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                double REINT_FISCAL = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("REINT_FISCAL")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();

                rubrique.setMontantPatronal(URSSAF_ASS_MAL_MAT_INV_DEC + URSSAF_ASS_MAL_MAT_INV_DEC_COMP + SANTE_PREV_A + SANTE_PREV_B + SANTE_MUT_OBL + URSSAF_ACC_TRAV + URSSAF_ASS_VIE_PLA + URSSAF_ASS_VIE_DEPLA + RET_AGIRC_ARRCO_T1 + RET_AGIRC_ARRCO_T2 + RET_CEG_T1 + RET_CEG_T2 + RET_CET + URSSAF_ALLOC_FAMI + URSSAF_ALLOC_FAMI_COMP + URSSAF_COTIS_CHOM + URSSAF_COTIS_AGS + RET_APEC + URSSAF_FNAL + URSSAF_CONTR_SOLIDARITE_AUTO + DIV_VER_TRANS + DIV_PART_EFFORT_CONSTR + URSSAF_CONTR_DIA_SOCI + URSSAF_FORFA_SOC + FOR_CONTR_UNIQUE_FPRO_APP + FOR_CONTR_SUP_APP + CCN_CONTR_ADESATT + URSSAF_CSG_NO_IMP + URSSAF_CSG_IMP + URSSAF_CRDS + REINT_FISCAL);


            }
        };
        return rubrique;
    }
    public Paie montantSalarial(Paie rubrique ,List<Paie> rubriquesPaie, Param param)
    {
        switch (rubrique.getFormuleMontantSalarial()) {
            case "assiette* taux_salarial"-> {
                rubrique.setMontantSalarial(rubrique.getAssiette()*rubrique.getTauxSalarial());
            }
            case "(SAL_BASE*(Nbj_Mois - Nbj_Presence) / Nbj_Mois)"-> {
                Optional<Paie> salaireBase = rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SAL_BASE")).findFirst();
                rubrique.setMontantSalarial(salaireBase.get().getMontantSalarial()*((param.getNbj_Mois()-param.getNbj_Presence())/param.getNbj_Mois()));
            }

            case "PRIME_VACANCES"-> {
                rubrique.setMontantSalarial(param.getPRIME_VACANCES());
            }
            case "PRIME_BRUT_1"-> {
                rubrique.setMontantSalarial(param.getPRIME_VACANCES());
            }
            case "PRIME_BRUT_2"-> {
                rubrique.setMontantSalarial(param.getPRIME_VACANCES());
            }

            case "SAL_BASE  - ENTRE_SORTIE  + PRIME_VACANCES + PRIME_BRUT_1 + PRIME_BRUT_2  + HEURES_SUP_25 + HEURES_SUP_50 + AVANTAGE_NATURE_1 + AVANTAGE_NATURE_2 + AVANTAGE_NATURE_3 + AVANTAGE_NATURE_4 - RET_ABS_CP + IND_ABS_CP - RET_ABS_MAL + IND_ABS_MAL - RET_ABS_AT  + IND_ABS_AT - RET_IJSS_TOTAL - RET_IJSS_MAINT_NET"-> {
                double SAL_BASE = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SAL_BASE")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double ENTRE_SORTIE = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("ENTRE_SORTIE")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double PRIME_VACANCES = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("PRIME_VACANCES")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double PRIME_BRUT_1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("PRIME_BRUT_1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double PRIME_BRUT_2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("PRIME_BRUT_2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double HEURES_SUP_25 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("HEURES_SUP_25")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double HEURES_SUP_50 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("HEURES_SUP_50")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_3 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_3")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_4 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_4")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double RET_ABS_CP  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_ABS_CP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double IND_ABS_CP  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("IND_ABS_CP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double IND_ABS_MAL  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("IND_ABS_MAL")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double RET_ABS_MAL  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_ABS_MAL")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double IND_ABS_AT  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("IND_ABS_AT")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double RET_ABS_AT  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_ABS_AT")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double RET_IJSS_TOTAL  = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("NB_IJSS_TOT")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double RET_IJSS_MAINT_NET   = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_IJSS_MAINT_NET")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                rubrique.setMontantSalarial(SAL_BASE  - ENTRE_SORTIE  + PRIME_VACANCES + PRIME_BRUT_1 + PRIME_BRUT_2  + HEURES_SUP_25 + HEURES_SUP_50 + AVANTAGE_NATURE_1 + AVANTAGE_NATURE_2 + AVANTAGE_NATURE_3 + AVANTAGE_NATURE_4 - RET_ABS_CP + IND_ABS_CP - RET_ABS_MAL + IND_ABS_MAL - RET_ABS_AT  + IND_ABS_AT - RET_IJSS_TOTAL - RET_IJSS_MAINT_NET);

            }

            case "SAL_BASE  - ENTRE_SORTIE  + PRIME_VACANCES + PRIME_BRUT_1 + PRIME_BRUT_2  + HEURES_SUP_25 + HEURES_SUP_50 + AVANTAGE_NATURE_1 + AVANTAGE_NATURE_2 + AVANTAGE_NATURE_3 + AVANTAGE_NATURE_4"-> {
                double SAL_BASE = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SAL_BASE")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double ENTRE_SORTIE = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("ENTRE_SORTIE")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double PRIME_VACANCES = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("PRIME_VACANCES")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double PRIME_BRUT_1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("PRIME_BRUT_1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double PRIME_BRUT_2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("PRIME_BRUT_2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double HEURES_SUP_25 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("HEURES_SUP_25")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double HEURES_SUP_50 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("HEURES_SUP_50")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_3 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_3")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_4 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_4")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();

                rubrique.setMontantSalarial(SAL_BASE  - ENTRE_SORTIE  + PRIME_VACANCES + PRIME_BRUT_1 + PRIME_BRUT_2  + HEURES_SUP_25 + HEURES_SUP_50 + AVANTAGE_NATURE_1 + AVANTAGE_NATURE_2 + AVANTAGE_NATURE_3 + AVANTAGE_NATURE_4);

            }

            case "SANTE_MUT_OBL:montant_patronal"-> {
                double SANTE_MUT_OBL = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_MUT_OBL")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                rubrique.setMontantPatronal(SANTE_MUT_OBL);

            }
            case "URSSAF_ASS_MAL_MAT_INV_DEC:montant_salarial + URSSAF_ASS_MAL_MAT_INV_DEC_COMP:montant_salarial + SANTE_PREV_A:montant_salarial + SANTE_PREV_B:montant_salarial + SANTE_MUT_OBL:montant_salarial + URSSAF_ACC_TRAV:montant_salarial + URSSAF_ASS_VIE_PLA:montant_salarial + URSSAF_ASS_VIE_DEPLA:montant_salarial + RET_AGIRC_ARRCO_T1:montant_salarial + RET_AGIRC_ARRCO_T2:montant_salarial + RET_CEG_T1:montant_salarial + RET_CEG_T2:montant_salarial + RET_CET:montant_salarial + URSSAF_ALLOC_FAMI:montant_salarial + URSSAF_ALLOC_FAMI_COMP:montant_salarial + URSSAF_COTIS_CHOM:montant_salarial + URSSAF_COTIS_AGS:montant_salarial + RET_APEC:montant_salarial + URSSAF_FNAL:montant_salarial + URSSAF_CONTR_SOLIDARITE_AUTO:montant_salarial + DIV_VER_TRANS:montant_salarial + DIV_PART_EFFORT_CONSTR:montant_salarial + URSSAF_CONTR_DIA_SOCI:montant_salarial + URSSAF_FORFA_SOC:montant_salarial + FOR_CONTR_UNIQUE_FPRO_APP:montant_salarial + FOR_CONTR_SUP_APP:montant_salarial + CCN_CONTR_ADESATT:montant_salarial + URSSAF_CSG_NO_IMP:montant_salarial + URSSAF_CSG_IMP:montant_salarial + URSSAF_CRDS:montant_salarial + REINT_FISCAL:montant_salarial"-> {
                double  URSSAF_ASS_MAL_MAT_INV_DEC = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_MAL_MAT_INV_DEC")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_ASS_MAL_MAT_INV_DEC_COMP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_MAL_MAT_INV_DEC_COMP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  SANTE_PREV_A = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_PREV_A")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  SANTE_PREV_B = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_PREV_B")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  SANTE_MUT_OBL = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_MUT_OBL")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_ACC_TRAV = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ACC_TRAV")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_ASS_VIE_PLA = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_VIE_PLA")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_ASS_VIE_DEPLA = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ASS_VIE_DEPLA")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  RET_AGIRC_ARRCO_T1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_AGIRC_ARRCO_T1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  RET_AGIRC_ARRCO_T2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_AGIRC_ARRCO_T2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  RET_CEG_T1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_CEG_T1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  RET_CEG_T2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_CEG_T2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  RET_CET = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_CET")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_ALLOC_FAMI = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ALLOC_FAMI")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_ALLOC_FAMI_COMP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_ALLOC_FAMI_COMP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_COTIS_CHOM = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_COTIS_CHOM")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_COTIS_AGS = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_COTIS_AGS")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  RET_APEC = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("RET_APEC")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_FNAL = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SAL_BASE")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_CONTR_SOLIDARITE_AUTO = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_FNAL")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  DIV_VER_TRANS = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("DIV_VER_TRANS")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  DIV_PART_EFFORT_CONSTR = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("DIV_PART_EFFORT_CONSTR")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_CONTR_DIA_SOCI = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CONTR_DIA_SOCI")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_FORFA_SOC = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_FORFA_SOC")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  FOR_CONTR_UNIQUE_FPRO_APP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("FOR_CONTR_UNIQUE_FPRO_APP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  FOR_CONTR_SUP_APP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("FOR_CONTR_SUP_APP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  CCN_CONTR_ADESATT = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("CCN_CONTR_ADESATT")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_CSG_NO_IMP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CSG_NO_IMP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_CSG_IMP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CSG_IMP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  URSSAF_CRDS = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CRDS")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double  REINT_FISCAL = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("REINT_FISCAL")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                rubrique.setMontantSalarial(URSSAF_ASS_MAL_MAT_INV_DEC + URSSAF_ASS_MAL_MAT_INV_DEC_COMP + SANTE_PREV_A + SANTE_PREV_B + SANTE_MUT_OBL + URSSAF_ACC_TRAV + URSSAF_ASS_VIE_PLA + URSSAF_ASS_VIE_DEPLA + RET_AGIRC_ARRCO_T1 + RET_AGIRC_ARRCO_T2 + RET_CEG_T1 + RET_CEG_T2 + RET_CET + URSSAF_ALLOC_FAMI + URSSAF_ALLOC_FAMI_COMP + URSSAF_COTIS_CHOM + URSSAF_COTIS_AGS + RET_APEC + URSSAF_FNAL + URSSAF_CONTR_SOLIDARITE_AUTO + DIV_VER_TRANS + DIV_PART_EFFORT_CONSTR + URSSAF_CONTR_DIA_SOCI + URSSAF_FORFA_SOC + FOR_CONTR_UNIQUE_FPRO_APP + FOR_CONTR_SUP_APP + CCN_CONTR_ADESATT + URSSAF_CSG_NO_IMP + URSSAF_CSG_IMP + URSSAF_CRDS + REINT_FISCAL);

            }
            case "AVANTAGE_NATURE_1"-> {
                rubrique.setMontantSalarial(param.getAVANTAGE_NATURE_1());
            }
            case "AVANTAGE_NATURE_2"-> {
                rubrique.setMontantSalarial(param.getAVANTAGE_NATURE_2());
            }
            case "AVANTAGE_NATURE_3"-> {
                rubrique.setMontantSalarial(param.getAVANTAGE_NATURE_3());
            }
            case "AVANTAGE_NATURE_4"-> {
                rubrique.setMontantSalarial(param.getAVANTAGE_NATURE_4());
            }

            case "FRAIS_KM- (AVANTAGE_NATURE_1 + AVANTAGE_NATURE_2 + AVANTAGE_NATURE_3 + AVANTAGE_NATURE_4)"-> {
                double FRAIS_KM = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("FRAIS_KM")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_1 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_1")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_2 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_2")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_3 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_3")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double AVANTAGE_NATURE_4 = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("AVANTAGE_NATURE_4")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                rubrique.setMontantSalarial(FRAIS_KM- (AVANTAGE_NATURE_1 + AVANTAGE_NATURE_2 + AVANTAGE_NATURE_3 + AVANTAGE_NATURE_4));

            }
            case "TOTAL_BRUT   - TOTAL_CHARGES:montant_salarial + URSSAF_CSG_IMP:montant_salarial + URSSAF_CRDS:montant_salarial+ SANTE_MUT_OBL:montant_patronal"-> {
                double TOTAL_BRUT = this.calculBrute(rubriquesPaie, param);
                double TOTAL_CHARGES_SAL = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("TOTAL_CHARGES_SAL")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double URSSAF_CSG_IMP = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CSG_IMP")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double URSSAF_CRDS = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("URSSAF_CRDS")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double SANTE_MUT_OBL = this.montantPatronal(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("SANTE_MUT_OBL")).findFirst().get(),rubriquesPaie,  param).getMontantPatronal();
                rubrique.setMontantSalarial(TOTAL_BRUT - TOTAL_CHARGES_SAL + URSSAF_CSG_IMP + URSSAF_CRDS+ SANTE_MUT_OBL);
            }
            case "BRUT_TOTAL - TOTAL_CHARGES:montant_salarial + DIVERS NET:montant_salarial - IR_PAS:montant_salarial"-> {
                double BRUT_TOTAL = this.calculBrute(rubriquesPaie, param);
                double TOTAL_CHARGES = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("TOTAL_CHARGES")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double DIV_NET = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("DIV_NET")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                double IR_PAS = this.montantSalarial(rubriquesPaie.stream().filter(p -> p.getCodeRubrique().contains("IR_PAS")).findFirst().get(),rubriquesPaie,  param).getMontantSalarial();
                rubrique.setMontantSalarial(BRUT_TOTAL - TOTAL_CHARGES+ DIV_NET - IR_PAS);
            }
        };
        return rubrique;
    }
}
