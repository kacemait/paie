package com.example.dsn.paie;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
//public class Param {
//    double PMSS;
//    double SMIC;
//    double TAUX_PAS;
//    double ETAB_MOINS_SAL;
//    double PRIME_VACANCES;
//    double PRIME_BRUT_1;
//    double PRIME_BRUT_2;
//    double HEURES_SUP_25;
//    double AVANTAGE_NATURE_1;
//    double AVANTAGE_NATURE_2;
//    double AVANTAGE_NATURE_3;
//    double AVANTAGE_NATURE_4;
//    double NB_CPN;
//    double NB_CPN1;
//    double NB_CPN2;
//    double NB_ANCI_N;
//    double NB_ANCI_N1;
//    double Maladie;
//    double Nbj_Presence;
//    double Nbj_Mois;
//    double FraisKm;
//}
public class Param {
    double PMSS;
    double SMIC;

    double ETAB_NB_SAL;
    double NB_HR_ETAB;
    double NB_JR_OUVRE_MOY;

    double SAL_MENS;
    double TAUX_PAS;


    double PRIME_VACANCES;
    double PRIME_BRUT_1;
    double PRIME_BRUT_2;
    double AVANTAGE_NATURE_1;
    double AVANTAGE_NATURE_2;
    double AVANTAGE_NATURE_3;
    double AVANTAGE_NATURE_4;
    double NB_HS_25_EMPLOYE;
    double NB_HS_50_EMPLOYE;

    public double getPMSS() {
        return PMSS;
    }

    public void setPMSS(double PMSS) {
        this.PMSS = PMSS;
    }

    public double getSMIC() {
        return SMIC;
    }

    public void setSMIC(double SMIC) {
        this.SMIC = SMIC;
    }

    public double getETAB_NB_SAL() {
        return ETAB_NB_SAL;
    }

    public void setETAB_NB_SAL(double ETAB_NB_SAL) {
        this.ETAB_NB_SAL = ETAB_NB_SAL;
    }

    public double getNB_HR_ETAB() {
        return NB_HR_ETAB;
    }

    public void setNB_HR_ETAB(double NB_HR_ETAB) {
        this.NB_HR_ETAB = NB_HR_ETAB;
    }

    public double getNB_JR_OUVRE_MOY() {
        return NB_JR_OUVRE_MOY;
    }

    public void setNB_JR_OUVRE_MOY(double NB_JR_OUVRE_MOY) {
        this.NB_JR_OUVRE_MOY = NB_JR_OUVRE_MOY;
    }

    public double getSAL_MENS() {
        return SAL_MENS;
    }

    public void setSAL_MENS(double SAL_MENS) {
        this.SAL_MENS = SAL_MENS;
    }

    public double getTAUX_PAS() {
        return TAUX_PAS;
    }

    public void setTAUX_PAS(double TAUX_PAS) {
        this.TAUX_PAS = TAUX_PAS;
    }

    public double getPRIME_VACANCES() {
        return PRIME_VACANCES;
    }

    public void setPRIME_VACANCES(double PRIME_VACANCES) {
        this.PRIME_VACANCES = PRIME_VACANCES;
    }

    public double getPRIME_BRUT_1() {
        return PRIME_BRUT_1;
    }

    public void setPRIME_BRUT_1(double PRIME_BRUT_1) {
        this.PRIME_BRUT_1 = PRIME_BRUT_1;
    }

    public double getPRIME_BRUT_2() {
        return PRIME_BRUT_2;
    }

    public void setPRIME_BRUT_2(double PRIME_BRUT_2) {
        this.PRIME_BRUT_2 = PRIME_BRUT_2;
    }

    public double getAVANTAGE_NATURE_1() {
        return AVANTAGE_NATURE_1;
    }

    public void setAVANTAGE_NATURE_1(double AVANTAGE_NATURE_1) {
        this.AVANTAGE_NATURE_1 = AVANTAGE_NATURE_1;
    }

    public double getAVANTAGE_NATURE_2() {
        return AVANTAGE_NATURE_2;
    }

    public void setAVANTAGE_NATURE_2(double AVANTAGE_NATURE_2) {
        this.AVANTAGE_NATURE_2 = AVANTAGE_NATURE_2;
    }

    public double getAVANTAGE_NATURE_3() {
        return AVANTAGE_NATURE_3;
    }

    public void setAVANTAGE_NATURE_3(double AVANTAGE_NATURE_3) {
        this.AVANTAGE_NATURE_3 = AVANTAGE_NATURE_3;
    }

    public double getAVANTAGE_NATURE_4() {
        return AVANTAGE_NATURE_4;
    }

    public void setAVANTAGE_NATURE_4(double AVANTAGE_NATURE_4) {
        this.AVANTAGE_NATURE_4 = AVANTAGE_NATURE_4;
    }

    public double getNB_HS_25_EMPLOYE() {
        return NB_HS_25_EMPLOYE;
    }

    public void setNB_HS_25_EMPLOYE(double NB_HS_25_EMPLOYE) {
        this.NB_HS_25_EMPLOYE = NB_HS_25_EMPLOYE;
    }

    public double getNB_HS_50_EMPLOYE() {
        return NB_HS_50_EMPLOYE;
    }

    public void setNB_HS_50_EMPLOYE(double NB_HS_50_EMPLOYE) {
        this.NB_HS_50_EMPLOYE = NB_HS_50_EMPLOYE;
    }

    public double getNB_PRESENCE_CAL() {
        return NB_PRESENCE_CAL;
    }

    public void setNB_PRESENCE_CAL(double NB_PRESENCE_CAL) {
        this.NB_PRESENCE_CAL = NB_PRESENCE_CAL;
    }

    public double getNB_MOIS_CAL() {
        return NB_MOIS_CAL;
    }

    public void setNB_MOIS_CAL(double NB_MOIS_CAL) {
        this.NB_MOIS_CAL = NB_MOIS_CAL;
    }

    public double getNB_CPN() {
        return NB_CPN;
    }

    public void setNB_CPN(double NB_CPN) {
        this.NB_CPN = NB_CPN;
    }

    public double getNB_CPN1() {
        return NB_CPN1;
    }

    public void setNB_CPN1(double NB_CPN1) {
        this.NB_CPN1 = NB_CPN1;
    }

    public double getNB_CPN2() {
        return NB_CPN2;
    }

    public void setNB_CPN2(double NB_CPN2) {
        this.NB_CPN2 = NB_CPN2;
    }

    public double getNB_ANCI_N() {
        return NB_ANCI_N;
    }

    public void setNB_ANCI_N(double NB_ANCI_N) {
        this.NB_ANCI_N = NB_ANCI_N;
    }

    public double getNB_ANCI_N1() {
        return NB_ANCI_N1;
    }

    public void setNB_ANCI_N1(double NB_ANCI_N1) {
        this.NB_ANCI_N1 = NB_ANCI_N1;
    }

    public double getNB_CP_PRIS() {
        return NB_CP_PRIS;
    }

    public void setNB_CP_PRIS(double NB_CP_PRIS) {
        this.NB_CP_PRIS = NB_CP_PRIS;
    }

    public double getNB_CP_INDEM() {
        return NB_CP_INDEM;
    }

    public void setNB_CP_INDEM(double NB_CP_INDEM) {
        this.NB_CP_INDEM = NB_CP_INDEM;
    }

    public double getNB_MAL_PRIS() {
        return NB_MAL_PRIS;
    }

    public void setNB_MAL_PRIS(double NB_MAL_PRIS) {
        this.NB_MAL_PRIS = NB_MAL_PRIS;
    }

    public double getNB_MAL_INDEM() {
        return NB_MAL_INDEM;
    }

    public void setNB_MAL_INDEM(double NB_MAL_INDEM) {
        this.NB_MAL_INDEM = NB_MAL_INDEM;
    }

    public double getNB_AT_PRIS() {
        return NB_AT_PRIS;
    }

    public void setNB_AT_PRIS(double NB_AT_PRIS) {
        this.NB_AT_PRIS = NB_AT_PRIS;
    }

    public double getNB_AT_INDEM() {
        return NB_AT_INDEM;
    }

    public void setNB_AT_INDEM(double NB_AT_INDEM) {
        this.NB_AT_INDEM = NB_AT_INDEM;
    }

    public double getNB_IJSS_TOT() {
        return NB_IJSS_TOT;
    }

    public void setNB_IJSS_TOT(double NB_IJSS_TOT) {
        this.NB_IJSS_TOT = NB_IJSS_TOT;
    }

    public double getNB_IJSS_NET() {
        return NB_IJSS_NET;
    }

    public void setNB_IJSS_NET(double NB_IJSS_NET) {
        this.NB_IJSS_NET = NB_IJSS_NET;
    }

    public double getNB_JR_DEPLACEMENT_FRAIS_KM() {
        return NB_JR_DEPLACEMENT_FRAIS_KM;
    }

    public void setNB_JR_DEPLACEMENT_FRAIS_KM(double NB_JR_DEPLACEMENT_FRAIS_KM) {
        this.NB_JR_DEPLACEMENT_FRAIS_KM = NB_JR_DEPLACEMENT_FRAIS_KM;
    }

    public double getDISTANCE_DOMICILE_MISSION() {
        return DISTANCE_DOMICILE_MISSION;
    }

    public void setDISTANCE_DOMICILE_MISSION(double DISTANCE_DOMICILE_MISSION) {
        this.DISTANCE_DOMICILE_MISSION = DISTANCE_DOMICILE_MISSION;
    }

    public double getFRAIS_KM_CV() {
        return FRAIS_KM_CV;
    }

    public void setFRAIS_KM_CV(double FRAIS_KM_CV) {
        this.FRAIS_KM_CV = FRAIS_KM_CV;
    }

    public double getDIV_NET() {
        return DIV_NET;
    }

    public void setDIV_NET(double DIV_NET) {
        this.DIV_NET = DIV_NET;
    }

    public double getNbj_Presence() {
        return Nbj_Presence;
    }

    public void setNbj_Presence(double nbj_Presence) {
        Nbj_Presence = nbj_Presence;
    }

    public double getNbj_Mois() {
        return Nbj_Mois;
    }

    public void setNbj_Mois(double nbj_Mois) {
        Nbj_Mois = nbj_Mois;
    }

    double NB_PRESENCE_CAL;
    double NB_MOIS_CAL;
    double NB_CPN;
    double NB_CPN1;
    double NB_CPN2;
    double NB_ANCI_N;
    double NB_ANCI_N1;


    double NB_CP_PRIS;
    double NB_CP_INDEM;

    double NB_MAL_PRIS;
    double NB_MAL_INDEM;
    double NB_AT_PRIS;
    double NB_AT_INDEM;

    double NB_IJSS_TOT;
    double NB_IJSS_NET;

    double NB_JR_DEPLACEMENT_FRAIS_KM;
    double DISTANCE_DOMICILE_MISSION;
    double FRAIS_KM_CV;
    double DIV_NET;
    double Nbj_Presence;
    double Nbj_Mois;
}
